const  express = require('express');
let app  = express();
const   bodyParser   =  require('body-parser');
const port = 7777;


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname +'/client'));

app.get('/', (req, res)=>{
    console.log('webapp');
    res.sendfile('index');
})


app.listen(port,()=>{
      console.log(`server running on ${port}`);
});