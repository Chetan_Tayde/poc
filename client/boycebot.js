var ACTIVE_NOTIFICATION_ID = null;
var tsc = {
  startBotForIssue: function(notificationId, articleIdToInvoke) {
    ACTIVE_NOTIFICATION_ID = notificationId;
    nanorep.floatingWidget.expand();
    nanorep.floatingWidget.showArticle(articleIdToInvoke);
  }
};
var config = {
  localStorageKbItemName: "nanorep.float.conversational.chrisgdemo.kb910200362"
};
!(function() {
  var data = JSON.parse(localStorage.getItem(config.localStorageKbItemName));
  if (data && data.widgetFloatingState) {
    data.widgetFloatingState.expanded = false;
    localStorage.setItem(config.localStorageKbItemName, JSON.stringify(data));
  }
  if (typeof XMLHttpRequest === "undefined") {
    XMLHttpRequest = function() {
      try {
        return new ActiveXObject("Msxml2.XMLHTTP.6.0");
      } catch (e) {}
      try {
        return new ActiveXObject("Msxml2.XMLHTTP.3.0");
      } catch (e) {}
      try {
        return new ActiveXObject("Microsoft.XMLHTTP");
      } catch (e) {}
      throw new Error("This browser does not support XMLHttpRequest.");
    };
  }
})();

!(function(t, e, o, c, n, a) {
  var s = (window.nanorep = window.nanorep || {});
  (s = s[e] = s[e] || {}),
    (s.apiHost = a),
    (s.host = n),
    (s.path = c),
    (s.account = t),
    (s.protocol = "https:" === location.protocol ? "https:" : "http:"),
    (s.on =
      s.on ||
      function() {
        (s._calls = s._calls || []), s._calls.push([].slice.call(arguments));
      });
  var p = s.protocol + "//" + n + c + o + "?account=" + t,
    l = document.createElement("script");
  (l.async = l.defer = !0),
    l.setAttribute("src", p),
    document.getElementsByTagName("head")[0].appendChild(l);
})(
  "chrisgdemo",
  "floatingWidget",
  "floating-widget.js",
  "/web/",
  "chrisgdemo.nanorep.co"
);

function executeOnce(callback) {}
nanorep.floatingWidget.on("expandWidget", function() {
  executeOnce = function(callback) {
    callback();
  };
});
nanorep.floatingWidget.on({
  init: function() {
    this.setConfigId("910201532");
    // this.setConfigId('901817782');
    this.setKB("910200362");
    this.preprocessBotResponse(function(response) {
      const indexes = [];
      for (let x in response.entities) {
        indexes.push(x);
      }
      if (
        indexes.length &&
        response.entities[Math.max(...indexes)].kind === "EMPLOYEECHATS"
      ) {
        const properties = response.entities[Math.max(...indexes)].properties;
        const messageType = properties.find(
          property => property.kind === "MESSAGETYPE"
        );
        const chatValue = properties.find(
          property => property.kind === "CHATVALUE"
        );
        const chatSequence = properties.find(
          property => property.kind === "CHATSEQUENCE"
        );
        const EmployeeId = properties.find(
          property => property.kind === "EMPLOYEEID"
        );
        if (messageType) {
          if (messageType.value === "json") {
            response.text = printHtmlTemplateResponse(
              EmployeeId.value,
              JSON.parse(chatValue.value)
            );
          } else if (messageType.value === "string") {
            response.text = chatValue.value;
          }
        }
      }
    });
  }
});
